---
layout: markdown_page
title: "Accounting"
---

## Accounts payable

NOTE: Vendor invoices are to be sent to accountspayable@gitlab.com

Steps to process an invoice to pay a consultant/vendor:

1. Upon receipt of vendor invoices:
    * File a .pdf copy of the invoice to dropbox\For Approval.
    * Notify manager of new invoices to be approved by forwarding the email from the vendor.
    * Invoices are to be approved based on signed agreements, contract terms, and or purchase orders.
    * After review, manager to reply to email with “Approved”. An audit trail is required and this email will serve this purpose.

1. On approval, move the invoice from dropbox\For Approval to dropbox\Inbox

1. Post the invoice through accounting system.  Before paying any vendor, be sure there is a W-9 on file for them (Inc.vendors only).

1. On a daily basis, generate an AP aging summary from the accounting system and identify invoices to be paid.

1. Initiate payment(s) through the bank (Comerica/Rabobank) and notify management that there is a pending payment.  Include a summary of invoices being paid.

1. Verify the payment has cleared the bank.

1. Upon verified payment of the invoice move the .pdf copy of the invoice from dropbox\Inbox to folder inbox\”vendor name”.

1. Post the payment through the accounting system.

## Commission Payment Process

1. Each sales person will receive their own calculation template.

1. Salesperson is to complete their monthly template four days (payroll will send reminder) prior to first payroll of the month. Upon completion, salesperson will ping a manager for review and approval.

1. Approving manager will ping accounting upon approval.

1. Accounting will review and reconcile paid vs unpaid invoices.

1. Accounting will note in calculation template the amounts to be paid in commision.

1. Accounting will ping payroll that commission calculation is complete.

1. Payroll will enter commission into TriNet.