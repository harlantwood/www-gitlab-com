---
layout: markdown_page
title: "Sales Process"
---
## Helpful links for prospects and customers

1. [GitLab subscription page](https://about.gitlab.com/subscription/)

1. [GitLab HA page](https://about.gitlab.com/high-availability/)

1. [EE repo members](https://gitlab.com/subscribers/gitlab-ee/team)

1. [Standard subscribers list](https://gitlab.com/groups/standard/members)

1. [GitLab CE issues list](https://gitlab.com/gitlab-org/gitlab-ce/issues)

1. [GitLab.com support forum](https://gitlab.com/gitlab-com/support-forum/issues)

1. [GitLab feedback tracker](http://feedback.gitlab.com/forums/176466-general)

1. [GitLab documentation](http://doc.gitlab.com/)

1. [Offer for university students](https://about.gitlab.com/2014/05/19/students-now-free/)

1. [YouTube page](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg)

1. [GitLab architecture for noobs (office analogy)](https://dev.gitlab.org/gitlab/gitlabhq/blob/master/doc/development/architecture.md)

1. [GitLab flow](https://about.gitlab.com/2014/09/29/gitlab-flow/)

1. [Free GitLab workshop on Platzi (Job as trainer)](https://courses.platzi.com/courses/git-gitlab/)

## Migrating from Highrise to Streak

1. All new emails go to Streak

1. Highrise pending deal: make box when a pending deal task is addressed

1. Highrise completed deal: make box when renewal is due (3 months in)

## Streak workflow

1. All leads get boxes

1. Leads transform to opportunities and are included in the forecast when they become Sales Qualified Leads

1. An organization is created in the organization pipeline when one company gets more than one opportunity

1. Opportunities are created for renewal and updates

1. Follow-up tasks are made just for opportunities

## Generating leads

[Types of leads](/handbook/sales-process/generating_leads).

## Creating deals

Every time a deal is created, a task is also created. The task succinctly describes [next action](/handbook/sales-process/creating_deals).

## Deal management process

Find out [how to manage deals](/handbook/sales-process/deal_management_process) with potential customers.

## Account management

Aspects to consider when [managing an account](/handbook/sales-process/account_management).

## Emailing

[Customer emailing guidelines](/handbook/sales-process/emailing).

## Accounting

[Accounting procedures](/handbook/sales-process/accounting).

## Licenses

Everything about [lincenses and EE access](/handbook/sales-process/licenses).

## Sales Ordering Process

### Recurly orders

1. Order comes in via email from recurly and will always have an invoice # on it.
1. Customer subscribes via Recurly (this happens automatically via the online credit card order form)
1. Check to see if Streak Box exists (or not) by clicking on the orange box in the Gmail horizontal toolbar and searching for the company as shown on the Recurly invoice. If one already exists, then scroll to the company and click ‘Add to existing box’. If one does not https://us5.admin.mailchimp.com/lists/members/add?id=107301exist, create a new box for the name on the invoice and click ‘Create new Box named’.
1. Add customer to sales sheet (include Recurly invoice date and invoice #). Hand enter every cell.
1. Add customer to Mailchimp GitLab Newsletter list (Customer automatically subscribed to Mailchimp Subscribers List) Need to use login/pw which is found in LP. Need customer first and last name, company name and email when you go there.
1. If standard subscriber: (note - you must have their gitlab.com user name to add them to the subscribers group. You must ask the customer for their user name)
    * Add to subscribers group (to add subscribers, you need access)
      1. Click the green button to the right “Add members”
      1. Copy and paste the customers GitLab.com user name into the field next to People.
      1. The customers login info will appear as a drop down which you select by clicking
      1. Then go down to the next field next to Group Access where the default access level is Guest. Click on the field and select Developer from the options
      1. Then click the green button immediately below “Add users to group”
    * Send onboarding email. If basic subscriber, do not add.
1. Generate license keys
    * Go to https://license.gitlab.com
    * Select the sign in with gitlab green button
    * Select the New License green button
    * Enter the Recurly subscription ID - follow these steps:
      1. Go to Recurly login and log in
      1. Select the Accounts tab at the top of the menu on the left
      1. Type in the name of the account in the search bar on the right
      1. Account will show up and select the account
      1. In the Subscriptions pane, select the More button
      1. Copy the portion of the URL after subscriptions/ (do not include the /). This portion is the Subscription ID
      1. Return to https://license.gitlab.com
      1. Paste the subscription ID (what you just copied from the URL) into the Recurly Subscription ID field (box)
      1. Select the Make license from Recurly subscription button
1. Mark deal as PO (deal won) in Streak **(the above steps must be completed, make a deal if none exist)**
1. If end of month, OK to add deal value of unprocessed deals with PO received to the top of agenda for reporting purposes
1. Make a task for the follow up (if needed)
1. Inform team during team call if any of the new orders are notable customers

### Bank Transfer

1. Sales receives an order (one of the three options):
    * a Purchase Order (PO) issued by the company
    * a signed quote (the quote that we sent).
    * an e-mail agreement (“we accept the quote/subscription/etc”) or any other written statement. If order is a signed quote or written agreement, email and ask if there is a PO pending and get the PO number
1. Add customer to Mailchimp Subscribers list
    * NOTE: if customer signed up through Recurly, they are automatically added to the Subscribers list)
    * Or, if customer is a renewal, you can skip this step and the next which is to add them to the newsletter)
1. Add customer to Mailchimp GitLab Newsletter list
1. Send invoice to customer with one of the two onboarding emails or a renewal email Forward Recurly invoice to customer as safeguard against going to spam. Also reinterate to sign up on GitLab.com and send user name.
1. Add customer to sales sheet (include invoice date and nr.)
1. If standard subscriber, add to subscribers group
1. Generate license keys
1. Mark opportunity as PO (deal won) in Streak **(the above steps must be completed, make an opportunity if none exist)**
1. If end of month, OK to add deal value of unprocessed deals with PO received to the top of agenda for reporting purposes
1. Make a task for the follow up (if needed)
1. Inform team during team call if any of the new orders are notable customers
